# About laravel-vue-todolist

This project is a todo list project using Vue.js as a frontend and Laravel as a backend to manage all the todo list tasks. This app will have 2 users admin and a normal user. All users will be able to create, read, update, and delete (CRUD).
